package homeworks.homework_2

import scala.math.BigInt

object recursion {

  /** *
   * Напишите функцию вычисления чисел фибоначи, используя хвостовую рекурсию @tailrec
   * */
  def fibonacci(n: BigInt): BigInt = {
    @annotation.tailrec
    def getTailRec(n: BigInt, prev: BigInt, cur: BigInt): BigInt = {
      if (n == 0) {
        cur
      } else {
        getTailRec(n - 1, prev = prev + cur, cur = prev)
      }
    }
    getTailRec(n, prev = 1, cur = 0)
  }
}
