package lectures.lecture_1

object variables extends App {

  lazy val k1 = {
    throw new Exception("boom")
  }
  val i = 0 // final

  //val i = 0
  var j = 0 // mutable

  k1

  // val k2 = throw new Exception("boom")
}
